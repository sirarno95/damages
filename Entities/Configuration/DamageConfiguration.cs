﻿namespace Entities.Configuration
{
    using Entities.Models;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using System;
    using System.Collections.Generic;

    public class DamageConfiguration : IEntityTypeConfiguration<Damage>
    {
        public void Configure(EntityTypeBuilder<Damage> builder)
        {
            var companies = new List<Damage>
            {
                new Damage
                {
                    Id = new Guid("e5577b04-b68b-44ea-8a44-6c075f1d72c8"),
                    Kind = DamageKind.Dent,
                    Location = "Neck",
                    CreatedAt = DateTime.Now,
                    CreatedBy = "Adam",
                    LastUpdatedAt = DateTime.Now,
                    LastUpdatedBy = "Adam"
                },
                new Damage
                {
                    Id = new Guid("08c539ce-6d18-49e1-b41a-de355de7d6c1"),
                     Kind = DamageKind.Scratch,
                    Location = "Hand",
                    Remarks = "Something....",
                    CreatedAt = DateTime.Now,
                    CreatedBy = "Adam",
                    LastUpdatedAt = DateTime.Now,
                    LastUpdatedBy = "Adam"
                }
            };
            builder.HasData(companies);
        }
    }
}
