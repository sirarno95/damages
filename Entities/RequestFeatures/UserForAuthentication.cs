﻿namespace Entities.RequestFeatures
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class UserForAuthentication
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}