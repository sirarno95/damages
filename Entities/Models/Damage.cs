﻿namespace Entities.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public enum DamageKind
    {
        Dent,
        Scratch
    }

    public class Damage
    {
        [Column("DamageId")]
        public Guid Id { get; set; }

        [Required(ErrorMessage = "Damage kind is a required field.")]
        [EnumDataType(typeof(DamageKind))]
        public DamageKind Kind { get; set; }

        [Required(ErrorMessage = "Location is a required field.")]
        public string Location { get; set; }

        public string Remarks { get; set; }

        public DateTime CreatedAt { get; set; }
        public string CreatedBy { get; set; }
        public DateTime LastUpdatedAt { get; set; }
        public string LastUpdatedBy { get; set; }

        public override string ToString()
        {
            return $"{ Id }, { Kind }, { Location }, { Remarks }";
        }
    }
}
