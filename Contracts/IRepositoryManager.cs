﻿namespace Contracts
{
    public interface IRepositoryManager
    {
        IDamageRepository Damage { get; }
        void Save();
    }
}
