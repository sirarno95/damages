﻿using Entities.Models;
using System;
using System.Collections.Generic;

namespace Contracts
{
    public interface IDamageRepository
    {
        IEnumerable<Damage> GetAllDamages();
        Damage GetDamage(Guid damageId);
        void CreateDamage(Damage damage);
        void DeleteDamage(Damage damage);
    }
}
