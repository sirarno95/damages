﻿namespace Contracts
{
    using Entities.RequestFeatures;
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading.Tasks;

    public interface IAuthenticationManager
    {
        Task<bool> ValidateUser(UserForAuthentication userForAuth); 
        Task<string> CreateToken();
    }
}
