﻿namespace Repository
{
    using Contracts;
    using Entities;
    using Entities.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    class DamageRepository : RepositoryBase<Damage>, IDamageRepository
    {
        public DamageRepository(RepositoryContext repositoryContext)
            : base(repositoryContext) { }

        public IEnumerable<Damage> GetAllDamages() => FindAll().ToList();

        public Damage GetDamage(Guid damageId) => FindByCondition(d => d.Id.Equals(damageId)).SingleOrDefault();

        public void CreateDamage(Damage damage) => Create(damage);

        public void DeleteDamage(Damage damage) => Delete(damage);
    }
}
