﻿namespace Repository
{
    using Contracts;
    using Entities;
    using System.Threading.Tasks;

    public class RepositoryManager : IRepositoryManager
    {
        private readonly RepositoryContext _repositoryContext;
        private IDamageRepository _damageRepository;
        public RepositoryManager(RepositoryContext repositoryContext)
        {
            _repositoryContext = repositoryContext;
        }

        public IDamageRepository Damage
        {
            get
            {
                if (_damageRepository == null)
                {
                    _damageRepository = new DamageRepository(_repositoryContext);
                }
                return _damageRepository;
            }
        }

        public void Save()
        {
            _repositoryContext.SaveChanges();
        }

        public Task SaveAsync()
        {
            return _repositoryContext.SaveChangesAsync();
        }
    }
}
