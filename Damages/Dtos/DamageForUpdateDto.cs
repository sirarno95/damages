﻿namespace Damages.Dtos
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class DamageForUpdateDto
    {
        public string Kind { get; set; }
        public string Location { get; set; }
        public string Remarks { get; set; }
    }
}
