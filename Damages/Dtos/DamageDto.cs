﻿namespace Damages.Dtos
{
    using System;

    class DamageDto
    {
        public Guid Id { get; set; }
        public string Kind { get; set; }
        public string Location { get; set; }
        public string Remarks { get; set; }
        
        
        public DateTime CreatedAt { get; set; }
        public string CreatedBy { get; set; }
        public DateTime LastUpdatedAt { get; set; }
        public string LastUpdatedBy { get; set; }
        
    }
}
