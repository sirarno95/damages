﻿namespace Damages.Dtos
{
    public class DamageForCreationDto
    {
        public string Kind { get; set; }
        public string Location { get; set; }
        public string Remarks { get; set; }
    }
}
