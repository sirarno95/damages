﻿namespace Damages.Controllers
{
    using AutoMapper;
    using Contracts;
    using Damages.Dtos;
    using Entities.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.JsonPatch;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using System.Collections.Generic;

    [Route("api/damages")]
    [ApiController]
    public class DamagesController : Controller
    {
        private readonly IRepositoryManager _repository;
        private readonly ILoggerManager _logger;
        private readonly IMapper _mapper;

        public DamagesController(IRepositoryManager repository, ILoggerManager logger, IMapper mapper)
        {
            _repository = repository;
            _logger = logger;
            _mapper = mapper;
        }

        [HttpGet(Name = "GetDamages")]
        public IActionResult GetDamages()
        {
            var damages = _repository.Damage.GetAllDamages();
            var damagesDto = _mapper.Map<IEnumerable<DamageDto>>(damages);

            _logger.LogInfo($"Get all damages.");

            return Ok(damagesDto);
        }

        [HttpGet("{id}", Name = "DamageById")]
        public IActionResult GetDamage(Guid id)
        {
            var damage = _repository.Damage.GetDamage(id);
            if (damage == null)
            {
                _logger.LogWarn($"Damage with id: {id} doesn't exist in the database.");
                return NotFound();
            }
            else
            {
                var damageDto = _mapper.Map<DamageDto>(damage);
                _logger.LogInfo($"Get a damage with id: {id}.");
                return Ok(damageDto);
            }
        }

        [HttpPost, Authorize(Roles = "Administrator, Manager")]
        public IActionResult CreateDamage([FromBody] DamageForCreationDto damage)
        {
            if (damage == null)
            {
                _logger.LogError("DamageForCreationDto object sent from client is null.");
                return BadRequest("DamageForCreationDto object is null");

            }

            Damage damageEntity = _mapper.Map<Damage>(damage);
            damageEntity.CreatedAt = DateTime.Now;
            damageEntity.CreatedBy = User.Identity.Name.ToString();


            _repository.Damage.CreateDamage(damageEntity);
            _repository.Save();

            _logger.LogInfo($"Successfully create a new damage by {User.Identity.Name}");

            var damageToReturn = _mapper.Map<DamageDto>(damageEntity);
            return CreatedAtRoute("DamageById", new { id = damageToReturn.Id }, damageToReturn);
        }

        [HttpPut("{id}"), Authorize(Roles = "Administrator, Manager")]
        public IActionResult UpdateDamage(Guid id, [FromBody] DamageForUpdateDto damageDto)
        {
            if (damageDto == null)
            {
                _logger.LogError("DamageForUpdateDto object sent from client is null.");
                return BadRequest("DamageForUpdateDto object is null");
            }

            var damageEntity = _repository.Damage.GetDamage(id);
            if (damageEntity == null)
            {
                _logger.LogWarn($"Damage with id: {id} doesn't exist in the database.");
                return NotFound();
            }

            _mapper.Map(damageDto, damageEntity);
            damageEntity.LastUpdatedAt = DateTime.Now;
            damageEntity.LastUpdatedBy = User.Identity.Name.ToString();
            _repository.Save();

            _logger.LogInfo($"Successfully update a damage by {User.Identity.Name}");

            return NoContent();
        }

        
        [HttpPatch("{id}"), Authorize(Roles = "Administrator, Manager")]
        public IActionResult PartiallyUpdateDamage(Guid id, [FromBody] JsonPatchDocument<DamageForUpdateDto> patchDoc)
        {
            if (patchDoc == null)
            {
                _logger.LogError("patchDoc object sent from client is null.");
                return BadRequest("patchDoc object is null");
            }

            var damageEntity =  _repository.Damage.GetDamage(id);

            var damageToPatch = _mapper.Map<DamageForUpdateDto>(damageEntity);

            patchDoc.ApplyTo(damageToPatch, ModelState);
            TryValidateModel(damageToPatch);

            if (!ModelState.IsValid)
            {
                _logger.LogError("Invalid model state for the patch document");
                return UnprocessableEntity(ModelState);
            }


            _mapper.Map(damageToPatch, damageEntity);
            damageEntity.LastUpdatedAt = DateTime.Now;
            damageEntity.LastUpdatedBy = User.Identity.Name.ToString();
            _repository.Save();

            _logger.LogInfo($"Successfully patch a damage by {User.Identity.Name}");

            return NoContent();
        }

        [HttpDelete("{id}"), Authorize(Roles = "Administrator")] 
        public IActionResult DeleteDamage(Guid id) 
        { 
            var damage = _repository.Damage.GetDamage(id);
            if (damage == null) 
            {
                _logger.LogWarn($"Damage with id: {id} doesn't exist in the database.");
                return NotFound();
            }

            _logger.LogInfo($"Successfully patch a damage by {User.Identity.Name}");

            _repository.Damage.DeleteDamage(damage);
            _repository.Save(); return NoContent(); }
    }

}
