﻿namespace Damages.Profiles
{
    using AutoMapper;
    using Entities.Models;
    using Damages.Dtos;
    using Entities.RequestFeatures;

    public class MappingProfile : Profile
    {
       public MappingProfile()
        {
            CreateMap<Damage, DamageDto>()
                .ForMember(
                d => d.Kind,
                opt => opt.MapFrom(x => x.Kind.ToString()));

            CreateMap<DamageForCreationDto, Damage>();

            CreateMap<DamageForUpdateDto, Damage>().ReverseMap();

            CreateMap<UserForRegistrationDto, User>();

            CreateMap<UserForAuthenticationDto, UserForAuthentication>();
        }
    }
}
