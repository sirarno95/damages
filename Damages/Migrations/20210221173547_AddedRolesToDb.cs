﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Damages.Migrations
{
    public partial class AddedRolesToDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "5e590d71-60ba-49c4-82b6-256435ea5a10", "6d3dd67a-817a-492d-93d3-cdd742292958", "Manager", "MANAGER" },
                    { "0ad84159-4488-4db3-ab80-c85428d42d62", "151834f3-4d39-4f5d-8865-d8e8dfe4dda5", "Administrator", "ADMINISTRATOR" }
                });

            migrationBuilder.UpdateData(
                table: "Damages",
                keyColumn: "DamageId",
                keyValue: new Guid("08c539ce-6d18-49e1-b41a-de355de7d6c1"),
                columns: new[] { "CreatedAt", "LastUpdatedAt" },
                values: new object[] { new DateTime(2021, 2, 21, 18, 35, 47, 409, DateTimeKind.Local).AddTicks(8894), new DateTime(2021, 2, 21, 18, 35, 47, 409, DateTimeKind.Local).AddTicks(8921) });

            migrationBuilder.UpdateData(
                table: "Damages",
                keyColumn: "DamageId",
                keyValue: new Guid("e5577b04-b68b-44ea-8a44-6c075f1d72c8"),
                columns: new[] { "CreatedAt", "LastUpdatedAt" },
                values: new object[] { new DateTime(2021, 2, 21, 18, 35, 47, 409, DateTimeKind.Local).AddTicks(7068), new DateTime(2021, 2, 21, 18, 35, 47, 409, DateTimeKind.Local).AddTicks(7823) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "0ad84159-4488-4db3-ab80-c85428d42d62");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "5e590d71-60ba-49c4-82b6-256435ea5a10");

            migrationBuilder.UpdateData(
                table: "Damages",
                keyColumn: "DamageId",
                keyValue: new Guid("08c539ce-6d18-49e1-b41a-de355de7d6c1"),
                columns: new[] { "CreatedAt", "LastUpdatedAt" },
                values: new object[] { new DateTime(2021, 2, 21, 18, 32, 34, 627, DateTimeKind.Local).AddTicks(2595), new DateTime(2021, 2, 21, 18, 32, 34, 627, DateTimeKind.Local).AddTicks(2621) });

            migrationBuilder.UpdateData(
                table: "Damages",
                keyColumn: "DamageId",
                keyValue: new Guid("e5577b04-b68b-44ea-8a44-6c075f1d72c8"),
                columns: new[] { "CreatedAt", "LastUpdatedAt" },
                values: new object[] { new DateTime(2021, 2, 21, 18, 32, 34, 627, DateTimeKind.Local).AddTicks(759), new DateTime(2021, 2, 21, 18, 32, 34, 627, DateTimeKind.Local).AddTicks(1525) });
        }
    }
}
