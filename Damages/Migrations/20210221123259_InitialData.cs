﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Damages.Migrations
{
    public partial class InitialData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Damages",
                keyColumn: "DamageId",
                keyValue: new Guid("08c539ce-6d18-49e1-b41a-de355de7d6c1"),
                columns: new[] { "CreatedAt", "LastUpdatedAt" },
                values: new object[] { new DateTime(2021, 2, 21, 13, 32, 58, 951, DateTimeKind.Local).AddTicks(3165), new DateTime(2021, 2, 21, 13, 32, 58, 951, DateTimeKind.Local).AddTicks(3191) });

            migrationBuilder.UpdateData(
                table: "Damages",
                keyColumn: "DamageId",
                keyValue: new Guid("e5577b04-b68b-44ea-8a44-6c075f1d72c8"),
                columns: new[] { "CreatedAt", "LastUpdatedAt" },
                values: new object[] { new DateTime(2021, 2, 21, 13, 32, 58, 951, DateTimeKind.Local).AddTicks(1328), new DateTime(2021, 2, 21, 13, 32, 58, 951, DateTimeKind.Local).AddTicks(2091) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Damages",
                keyColumn: "DamageId",
                keyValue: new Guid("08c539ce-6d18-49e1-b41a-de355de7d6c1"),
                columns: new[] { "CreatedAt", "LastUpdatedAt" },
                values: new object[] { new DateTime(2021, 2, 21, 13, 32, 42, 330, DateTimeKind.Local).AddTicks(6758), new DateTime(2021, 2, 21, 13, 32, 42, 330, DateTimeKind.Local).AddTicks(6787) });

            migrationBuilder.UpdateData(
                table: "Damages",
                keyColumn: "DamageId",
                keyValue: new Guid("e5577b04-b68b-44ea-8a44-6c075f1d72c8"),
                columns: new[] { "CreatedAt", "LastUpdatedAt" },
                values: new object[] { new DateTime(2021, 2, 21, 13, 32, 42, 330, DateTimeKind.Local).AddTicks(4916), new DateTime(2021, 2, 21, 13, 32, 42, 330, DateTimeKind.Local).AddTicks(5678) });
        }
    }
}
