﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Damages.Migrations
{
    public partial class DatabaseCreation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Damages",
                columns: table => new
                {
                    DamageId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Kind = table.Column<int>(type: "int", nullable: false),
                    Location = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Remarks = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LastUpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    LastUpdatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Damages", x => x.DamageId);
                });

            migrationBuilder.InsertData(
                table: "Damages",
                columns: new[] { "DamageId", "CreatedAt", "CreatedBy", "Kind", "LastUpdatedAt", "LastUpdatedBy", "Location", "Remarks" },
                values: new object[] { new Guid("e5577b04-b68b-44ea-8a44-6c075f1d72c8"), new DateTime(2021, 2, 21, 13, 32, 42, 330, DateTimeKind.Local).AddTicks(4916), "Adam", 0, new DateTime(2021, 2, 21, 13, 32, 42, 330, DateTimeKind.Local).AddTicks(5678), "Adam", "Neck", null });

            migrationBuilder.InsertData(
                table: "Damages",
                columns: new[] { "DamageId", "CreatedAt", "CreatedBy", "Kind", "LastUpdatedAt", "LastUpdatedBy", "Location", "Remarks" },
                values: new object[] { new Guid("08c539ce-6d18-49e1-b41a-de355de7d6c1"), new DateTime(2021, 2, 21, 13, 32, 42, 330, DateTimeKind.Local).AddTicks(6758), "Adam", 1, new DateTime(2021, 2, 21, 13, 32, 42, 330, DateTimeKind.Local).AddTicks(6787), "Adam", "Hand", "Something...." });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Damages");
        }
    }
}
